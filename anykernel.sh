# AnyKernel3 Ramdisk Mod Script
# osm0sis @ xda-developers

# Checking...
properties() { '
kernel.string=dimasyudha kernel by @dimasyudha
do.devicecheck=1
do.modules=0
do.systemless=1
do.cleanup=1
do.cleanuponabort=1
device.name1=sirius
supported.versions=10-11
'; }

# Shell variables
block=/dev/block/bootdevice/by-name/boot;
is_slot_device=0;
ramdisk_compression=auto;
no_block_display=true;

# Import patching variables
. tools/ak3-core.sh;

# Begin install
dump_boot;

# End install
write_boot;
